import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

export default class CustomPath extends LightningElement {
    @api div1Class = '';
    @api div2Class = 'slds-path';
    @api div3Class = 'slds-grid slds-path__track';
    @api div4Class = 'slds-grid slds-path__scroller-container';
    @api div5Class = 'slds-path__scroller';
    @api div6Class = 'slds-path__scroller_inner';
    @api olClass = 'slds-path__nav'; 
    @api olRole = 'listbox';
    @api olAriaOrientation = 'horizontal';
    @api liClass = 'slds-path__item slds-is-incomplete';
    @api liRole = 'presentation';
    @api div7Class = 'slds-path__link';
    @api option = 'option';
    @api spanClass = 'slds-path__stage';
    @api spanAssistClass = 'slds-assistive-text';
    @api spanTitleClass = 'slds-path__title';
    @api iconName = 'action:approval';
    @api iconAltText = 'Stage Complete';
    @api iconSize = 'x-small';
    @api pathItems;
    @api links = false;
    
    @wire(CurrentPageReference) pageRef;

    onClick(event) {
        let pathId = event.target.id;
        fireEvent(this.pageRef, 'click', pathId);
    }   

    onKeyUp(event) {
        let pathId = event.target.id;
        let keyUp = event.code;
        let eventObject = {
            pathId: pathId,
            keyUp: keyUp
        }
        fireEvent(this.pageRef, 'keyup', eventObject);
    }   
}