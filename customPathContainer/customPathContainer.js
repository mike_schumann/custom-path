import { LightningElement, wire } from 'lwc';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';

export default class CustomPathContainer extends LightningElement {
    current = 'slds-path__item slds-is-current slds-is-active';
    incomplete = 'slds-path__item slds-is-incomplete';
    complete = 'slds-path__item slds-is-complete';
    closed = 'slds-path__item slds-is-lost slds-is-active slds-is-current';
    olClass = 'slds-path__nav olClass';
    index = 0;
    previousSelected = 0;
    pathItems = [{
        id: 0,
        liClass: this.current,
        ariaSelected: true,
        tabIndex: '0',
        assistiveText: 'Stage Complete',
        title: 'Title One',
        link: 'https://google.com',
        target: '_blank',
        referrerpolicy: 'no-referrer',
        rel: 'noopener noreferrer'
    },
    {
        id: 1,
        liClass: this.incomplete,
        ariaSelected: false,
        tabIndex: '0',
        assistiveText: 'Stage Complete',
        title: 'Title Two',
        link: 'https://google.com',
        target: '_blank',
        referrerpolicy: 'no-referrer',
        rel: 'noopener noreferrer'
    },
    {
        id: 2,
        liClass: this.incomplete,
        ariaSelected: false,
        tabIndex: '0',
        assistiveText: 'Stage Complete',
        title: 'Title Three',
        link: 'https://google.com',
        target: '_blank',
        referrerpolicy: 'no-referrer',
        rel: 'noopener noreferrer'
    },
    {
        id: 3,
        liClass: this.incomplete,
        ariaSelected: false,
        tabIndex: '0',
        assistiveText: 'Stage Complete',
        title: 'Title Four',
        link: 'https://google.com',
        target: '_blank',
        referrerpolicy: 'no-referrer',
        rel: 'noopener noreferrer'
    },
    {
        id: 4,
        liClass: this.incomplete,
        ariaSelected: false,
        tabIndex: '0',
        assistiveText: 'Stage Complete',
        title: 'Title Five',
        link: 'https://google.com',
        target: '_blank',
        referrerpolicy: 'no-referrer',
        rel: 'noopener noreferrer'
    },
    {
        id: 5,
        liClass: this.incomplete,
        ariaSelected: false,
        tabIndex: '0',
        assistiveText: 'Stage Complete',
        title: 'Title Six jlasjdlfjsl asdjlfkjas dfl jl',
        link: 'https://google.com',
        target: '_blank',
        referrerpolicy: 'no-referrer',
        rel: 'noopener noreferrer'
    },
    {
        id: 6,
        liClass: this.incomplete,
        ariaSelected: false,
        tabIndex: '0',
        assistiveText: 'Stage Complete',
        title: 'Title Seven',
        link: 'https://google.com',
        target: '_blank',
        referrerpolicy: 'no-referrer',
        rel: 'noopener noreferrer'
    },
    {
        id: 7,
        liClass: this.incomplete,
        ariaSelected: false,
        tabIndex: '0',
        assistiveText: 'Stage Complete',
        title: 'Title Eight Very Long Title Name Continued jlajlkjsdlkfjd jkajs dfp joasdifj jpadjf ;lakjsdfp ',
        link: 'https://google.com',
        target: '_blank',
        referrerpolicy: 'no-referrer',
        rel: 'noopener noreferrer'
    }];
    previousDisable = true;
    nextDisable = this.pathItems.length > 1 ? false : true;

    @wire(CurrentPageReference) pageRef;
    connectedCallback() {
        registerListener('click', this.getPathId, this);
        registerListener('keyup', this.checkKeyUpCode, this);
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    handlePrevious() {
        let previousIndex = this.index;
        this.index = this.index - 1;
        if (this.index <= 0) {
            this.previousDisable = true;
            this.index = 0;
            this.pathItems[this.index].liClass = this.current;
            this.pathItems[this.index].ariaSelected = true;
            this.pathItems[previousIndex].liClass = this.incomplete;
            this.pathItems[previousIndex].ariaSelected = false;
            if (this.previousSelected != this.index) {
                this.pathItems[this.previousSelected].liClass = this.pathItems[this.previousSelected].liClass.replace('slds-is-active', '');
                this.pathItems[this.previousSelected].ariaSelected = false;
            }   
            this.pathItems = [].concat(this.pathItems);
        }
        else {
            this.previousDisable = false;
            this.nextDisable = false;
            this.pathItems[this.index].liClass = this.current;
            this.pathItems[this.index].ariaSelected = true;
            this.pathItems[previousIndex].liClass = this.incomplete;
            this.pathItems[previousIndex].ariaSelected = false;
            if (this.previousSelected != this.index) {
                this.pathItems[this.previousSelected].liClass = this.pathItems[this.previousSelected].liClass.replace('slds-is-active', '');
                this.pathItems[this.previousSelected].ariaSelected = false;
            }    
            this.pathItems = [].concat(this.pathItems);
        }
        this.previousSelected = this.index;
    };

    handleNext() {
        let previousIndex = this.index;
        this.index++;
        if (this.index >= this.pathItems.length - 1) {
            this.nextDisable = true;
            this.index = this.pathItems.length - 1;
            this.pathItems[this.index].liClass = this.closed;
            this.pathItems[this.index].ariaSelected = true;
            this.pathItems[previousIndex].liClass = this.complete;
            this.pathItems[previousIndex].ariaSelected = false;
            if (this.previousSelected != this.index) {
                this.pathItems[this.previousSelected].liClass = this.pathItems[this.previousSelected].liClass.replace('slds-is-active', '');
                this.pathItems[this.previousSelected].ariaSelected = false;
            }    
            this.pathItems = [].concat(this.pathItems);
        }
        else {
            this.nextDisable = false;
            this.previousDisable = false;
            this.pathItems[this.index].liClass = this.current;
            this.pathItems[this.index].ariaSelected = true;
            this.pathItems[previousIndex].liClass = this.complete;
            this.pathItems[previousIndex].ariaSelected = false;
            if (this.previousSelected != this.index) {
                this.pathItems[this.previousSelected].liClass = this.pathItems[this.previousSelected].liClass.replace('slds-is-active', '');
                this.pathItems[this.previousSelected].ariaSelected = false;
            }           
            this.pathItems = [].concat(this.pathItems);
        }
        this.previousSelected = this.index;
    };

    getPathId(pathId) {
        let currentIndex = this.index;
        let targetIndex = pathId.slice(0,1).valueOf();
        let previousIndex = this.previousSelected;
        if (targetIndex == currentIndex) {
            if (this.pathItems[currentIndex].liClass.includes('active')) {
                this.pathItems[previousIndex].liClass = this.pathItems[previousIndex].liClass.replace('slds-is-active', '');
            }
            else {
                this.pathItems[currentIndex].liClass = this.pathItems[currentIndex].liClass + ' slds-is-active';
                this.pathItems[previousIndex].liClass = this.pathItems[previousIndex].liClass.replace('slds-is-active', '');
                this.pathItems = [].concat(this.pathItems);
            }
        }
        else {
            this.pathItems[currentIndex].ariaSelected = false;
            this.pathItems[currentIndex].liClass = 'slds-path__item slds-is-current';
            this.pathItems[targetIndex].ariaSelected = true;
            this.pathItems[targetIndex].liClass = this.pathItems[targetIndex].liClass + ' slds-is-active';
            
            if (previousIndex != currentIndex) {
                this.pathItems[previousIndex].ariaSelected = false;
                this.pathItems[previousIndex].liClass = this.pathItems[previousIndex].liClass.replace('slds-is-active', '');
                
            }            
            this.pathItems = [].concat(this.pathItems);            
        }
        this.previousSelected = targetIndex;        
    }

    checkKeyUpCode(eventObject) {
        if (eventObject.keyUp === 'Space') {
            this.getPathId(eventObject.pathId);
        }
    }
}