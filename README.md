# Custom Path LWC #

### The problem ###

* Standard out of the box path componnt is not customizable.
* Standeard out of the box path component does not meet accessibility guidelines and requirements.

### Setup ###

* Move the customPath component into your LWC folder. 
* Create the customPathCSSLibrary component in your LWC folder. This will be a lwc with only a CSS file in it. If any other files are included in the customPathCSSLibrary the css import will not work correctly. An example of the customPathCSSLibrary is included in this repsitory.
* You will need to build a parent component to manage the path items and the navigation of the path. POC for navigation from parent component is included in this repository. Navigation is controlled by modifying the slds classes of the path items.

### Component attributes ###

* The attributes below are the current defaults for the component so that it will render the path with limited data from the parent. That means at a minimum the component needs to be passed the path items to display. 

* The first section attributes are in the html order as they appear in the component starting from outer to inner html. 
* The default classes can be overwrriten with  slds classes from the Path classes from the lightning design system: https://www.lightningdesignsystem.com/components/path/
* Custom classes can also be passed in with the slds classes to provide greater flexibility for design.
	* @api div1Class = '';
    * @api div2Class = 'slds-path';
    * @api div3Class = 'slds-grid slds-path__track';
    * @api div4Class = 'slds-grid slds-path__scroller-container';
    * @api div5Class = 'slds-path__scroller';
    * @api div6Class = 'slds-path__scroller_inner';
    * @api olClass = 'slds-path__nav'; 
    * @api olRole = 'listbox';
    * @api olAriaOrientation = 'horizontal';
    * @api liClass = 'slds-path__item slds-is-incomplete';
    * @api liRole = 'presentation';
    * @api div7Class = 'slds-path__link';
    * @api option = 'option';
    * @api spanClass = 'slds-path__stage';
    * @api spanAssistClass = 'slds-assistive-text';
    * @api spanTitleClass = 'slds-path__title';
	
* These attributes pertain to the icon check mark within the path item. You can edit the alt text and size of the check mark. 
    * @api iconName = 'action:approval';
    * @api iconAltText = 'Stage Complete';
    * @api iconSize = 'x-small';
    
* links is a boolean attribute that is defaulted to false. If true it will change the html structure of the path to include <a> tags so that links can be provided in the pathItems.
    * @api links = false;
	
* pathItems is an array [] that provides the data for the path to render. 
    * @api pathItems;
	* Example of pathItems construction: 
		* let pathItems = [{
        * id: 0,
        * iClass: this.current,
        * ariaSelected: true,
        * tabIndex: '0',
        * assistiveText: 'Stage Complete',
        * title: 'Title One',
        * link: 'https://google.com',
        * target: '_blank',
        * referrerpolicy: 'no-referrer',
        * rel: 'noopener noreferrer'
    * }];
	* Additional objects can be added to the array and each object represents a path item.
    * Attributes link, target, referrerpolicy, and rel are only required if the links attribute is true. 
    * The target attribute allows you determine whether the link opens in the same tab or a new tab.
    * The referrerpolicy and rel attributes are required when using links but are needed when opening a new tab outside your window for security reasons and to prevent phishing or tab hijicking. 
	
    
* Events: There are two events that you can listen to using the pub/sub model.
    * click - This event returns the id of the path item. 
    * keyup - This event returns an object with the following keys:
        * keyup - The value is the key struck on the keyboard so that we can navigate by keyboard navigation.
        * pathId - The value is the path item id.
	
### POC ###

* Please review the customPathContainer component to see how navigation from outside the component can be accomplished. 
* The customPathContainer can be run in the local dev server for you to try out yourself.

### Questions ###

* Owner/Admin for Repo: Michael Schumann
* Email: michael.s@mstsolutions.com